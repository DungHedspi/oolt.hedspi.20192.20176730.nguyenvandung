package Shopping;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder = new Order();
		anOrder.setQtyOrdered(0);
		DitalVideoDisc dvd1 = new DitalVideoDisc("xxx");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		if(anOrder.addDigitalVideoDisc(dvd1)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		
		//dvd2
		DitalVideoDisc dvd2 = new DitalVideoDisc("xxy");
		dvd2.setCategory("Animation");
		dvd2.setCost(19.95f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(87);
		if(anOrder.addDigitalVideoDisc(dvd2)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		//dvd3
		DitalVideoDisc dvd3 = new DitalVideoDisc("xxz");
		dvd3.setCategory("Animation");
		dvd3.setCost(19.95f);
		dvd3.setDirector("Roger Allers");
		dvd3.setLength(87);
		if(anOrder.addDigitalVideoDisc(dvd3)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		
		if(anOrder.removeDigitalVideoDisc(dvd1)) {
			System.out.println("Delete Complete");
		}else {
			System.out.println("Delete Fail");
		}
		
		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());
	}

}
