package date;
import java.util.*;
public class MyDate {
	public static String [] CONST_MON = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};	
	private int year, day , mon;
	public MyDate() {
	
	}
	
	public MyDate(int year, int day, int mon) {
		this.year = year;
		this.day = day;
		this.mon = mon;
	}
	
	public MyDate(String str) {
		// tách ta lấy chuỗi
		
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMon() {
		return mon;
	}

	public void setMon(int mon) {
		this.mon = mon;
	}
	public boolean check(int day, int mon, int year) {
		boolean check = true;
		if(day <=0 || day > 31 ||  mon <=0|| mon >12 || year <0) check = false;
		else {
			switch (mon){
			case 4:
			case 6:
			case 9:
			case 11:{
				if(day == 31) check = false;
			} break;
			case 2:{
				if(year % 4 !=0 && year % 400 !=0) {
					check = day >28 ? false: true; 
				}else {
					check = day >29 ? false: true; 
				}
			}
		}
	}
		return check;
	}
	public int convertInteger(String str) {
		int num = 0;
		boolean status = false;
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i)>='0' && str.charAt(i)<='9' && !status) {
				num = num *10;
				num += str.charAt(i) - '0';
			}else {
				status = true;
			}
		}
		return num;
	}
	public boolean accpet() {
		boolean result = true;
		Scanner keyBroad = new Scanner(System.in);
		System.out.printf("Nhap chuoi ngay:");
		String strDate = keyBroad.nextLine(); 
		// Tach sau February 18th 2019
		String [] word = strDate.split("\\s");
		int day, mon = 0, year;
		for(int i = 0; i < CONST_MON.length; i++ ) {
			if(CONST_MON[i].equalsIgnoreCase(word[0])) {
				mon = i+1;
			}
		}
		day = convertInteger(word[1]);
		year = convertInteger(word[2]);
		if(check(day, mon, year)) {
			this.setDay(day);
			this.setMon(mon);
			this.setYear(year);
			result = true;
		}
		return result;
	}
	public String printDate() {
		return CONST_MON[this.getMon()-1]+" " + this.getDay() +" "+ this.getYear();
	}
}
