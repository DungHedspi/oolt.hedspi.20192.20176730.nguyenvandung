package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.media;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
	@SuppressWarnings("null")
	public static void main(String[] args) {
		@SuppressWarnings({ "resource" })
		Scanner keyBoard = new Scanner(System.in);
		int c = 0;
		Order order = null;
		do {
			 showMenu();
			
			 c = Integer.parseInt(keyBoard.nextLine());
			 switch(c) {
			 
			 	case 1:{
			 		System.out.println("-------------------------");
						 order = new Order();
						System.out.println("Created new item successfully");
			 	}break;
			 
			 	case 2:{
			 		System.out.println();
					System.out.println("Add item to the order");
					System.out.println("-------------------------");
					System.out.print("Enter title: ");
					String title = keyBoard.nextLine();
					System.out.print("Enter category: ");
					String category = keyBoard.nextLine();
					System.out.print("Enter cost: ");
					float cost = Float.parseFloat(keyBoard.nextLine());
					media item = new media(title, category, cost);
					boolean result = order.addMedia(item);
					if(result) {
						System.out.println("Add item to the order success");
					}else {
						System.out.println("Add item to the order failed");
					}
			 	}break;
			 
			 	case 3:{
			 		System.out.println();
					System.out.println("Delete item by id");
					System.out.print("Enter the index you want to delete: ");
					int index = keyBoard.nextInt();
					boolean result = order.removeMedia(index);
					if(result) {
						System.out.println("Remove item to the order success");
					}else {
						System.out.println("Remove item to the order failed");
					}
			 	}break;
			 	case 4:{
			 		System.out.println("items list of order");
			 		order.printOrder();
			 	}break;
			 	default: {
			 		System.out.println("^-^  ^-^  ^-^  ^-^");
			 	}break;
			 }	 
		}while(c == 1 || c==2 || c==3 || c==4);
		
	}

}
