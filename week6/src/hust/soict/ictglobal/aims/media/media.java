package hust.soict.ictglobal.aims.media;

public class media {
	
	private String title;
	private String category;
	private float cost;
	public media() {
		
	}
	public media(String title){
		this.title = title;
		}
	public media(String title, String category){
		this(title);
		this.category = category;
	}
	public media(String title, String category, float cost){
		this(title, category);
		this.cost = cost;
	}
	
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
}
