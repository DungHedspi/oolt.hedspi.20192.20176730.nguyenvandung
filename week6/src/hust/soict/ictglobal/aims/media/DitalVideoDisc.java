package hust.soict.ictglobal.aims.media;

public class DitalVideoDisc extends media{
	private String director;
	private int length;
	public DitalVideoDisc() {
		super();
	}

	public DitalVideoDisc(String title) {
		super(title);
	}

	public DitalVideoDisc(String title, String category) {
		super(title, category);

	}
	
	public DitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}

	public DitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}


	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
