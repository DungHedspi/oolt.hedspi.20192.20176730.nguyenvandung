package hust.soict.ictglobal.garbage;
import java.nio.file.*;
import java.util.Scanner; 
public class GarbageCreator {
	  
	 public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    Scanner sc = new Scanner(Paths.get(fileName),"UTF-8");
	    while(sc.hasNextLine()){
	    	String tmp = sc.nextLine();
	    	data += tmp;
	    }
	    sc.close();
	    return data; 
	  } 
	  
	  public static void main(String[] args) throws Exception 
	  { 
		long start = System.currentTimeMillis();
	    String data = readFileAsString("/home/ddung/Documents/parser.c"); 
	    System.out.println(data); 
	    System.out.println(System.currentTimeMillis()-start);
	  } 
} 