package hust.soict.ictglobal.garbage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class NoGarbage {
	public static void main(String[] args)throws Exception 
	  {
	  long start = System.currentTimeMillis();
	  File file = new File("/home/ddung/Documents/parser.c"); 
	  
	  BufferedReader br = new BufferedReader(new FileReader(file)); 
	  
	  String st=""; 
	  String tmp;
	  while ((tmp = br.readLine()) != null) 
	    st += tmp; 
	  
	  System.out.println(st);
	  System.out.println(System.currentTimeMillis()-start);
	  } 
}
