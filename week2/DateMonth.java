import java.util.Scanner;
public class DatemMonth{
    public static void main(String[] args){
        Scanner keyBoard = new Scanner(System.in);
        System.out.println("Nhap vao thang:");
        int Month = keyBoard.nextInt();
        while(Month <0 || Month >12){
            System.out.println("Khong hop le,Nhap vao thang:");
            Month = keyBoard.nextInt();
        }
        System.out.println("Nhap vao nam:");
        long year = keyBoard.nextInt();
        while( year <0){
            System.out.println("Khong hop le,Nhap vao nam:");
            year = keyBoard.nextLong();
        }
        System.out.println("Month:" + Month + ", Year:" + year);
        System.exit(-1);
    }
}