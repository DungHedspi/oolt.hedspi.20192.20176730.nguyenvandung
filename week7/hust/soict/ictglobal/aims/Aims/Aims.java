package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	static Order order = null;
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
	public static DigitalVideoDisc InputDVD(){
		DigitalVideoDisc dvd = new DigitalVideoDisc();
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		dvd.setTitle(sc.nextLine());
		System.out.print("Length: ");
		dvd.setLength(Integer.parseInt(sc.nextLine()));
		System.out.print("Cost: ");
		dvd.setCost(Float.parseFloat(sc.nextLine()));
		System.out.print("Category: ");
		dvd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		dvd.setDirector(sc.nextLine());
		return dvd;
	}
	public static CompactDisc InputCD(){
		CompactDisc cd = new CompactDisc();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		cd.setTitle(sc.nextLine());
		System.out.print("Category: ");
		cd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		cd.setDirector(sc.nextLine());
		System.out.print("Artist: ");
		cd.setArtist(sc.nextLine());
		System.out.print("Cost: ");
		cd.setCost(Float.parseFloat(sc.nextLine()));
		int flag=1;
		int i=1;
		do{
			if(flag==1){
				Track tr = new Track();
				System.out.print("Title of track "+i+": ");
				tr.setDirector(sc.nextLine());
				System.out.print("Length of track "+i+": ");
				tr.setLength(Integer.parseInt(sc.nextLine()));
				cd.addTrack(tr);
				i++;
			}
			
			System.out.print("Enter flag= ");
			flag= Integer.parseInt(sc.nextLine());
		}while(flag==1);
		return cd;
	}
	public static Book InputBook(){
		Book book= new Book();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		book.setTitle(sc.nextLine());
		System.out.print("Category: ");
		book.setCategory(sc.nextLine());
		System.out.print("Cost: ");
		book.setCost(Float.parseFloat(sc.nextLine()));
		int flag=1;
		int i=1;
		do{
			if(flag==1){
				System.out.print("Author "+i+": ");
				String author= sc.nextLine();
				book.addAuthor(author);
				i++;
			}		
			System.out.print("Enter flag= ");
			flag= Integer.parseInt(sc.nextLine());
		}while(flag==1);
		return book;
	}
	public static void addItemToOrder(DigitalVideoDisc dvd){
		order.addMedia(dvd);
	}
	public static void addItemToOrder(CompactDisc cd){
		order.addMedia(cd);
	}
	public static void addItemToOrder(Book book){
		order.addMedia(book);
	}
	@SuppressWarnings("null")
	public static void main(String[] args) {
		@SuppressWarnings({ "resource" })
		Scanner keyBoard = new Scanner(System.in);
		int c = 0;
		
		do {
			 showMenu();
			
			 c = Integer.parseInt(keyBoard.nextLine());
			 switch(c) {
			 
			 	case 1:{
			 		System.out.println("-------------------------");
						 order = new Order();
						System.out.println("Created new item successfully");
			 	}break;
			 
			 	case 2:{
			 	int code;
		    	System.out.println("Nhap code");
		    	@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);
		    	System.out.printf("1.Add DVD\n2.Add CompactDvD\n3.Add Book\n: please chose:");
		    	code = Integer.parseInt(sc.nextLine());
		    	if(code==1){
		    		DigitalVideoDisc dvd;
		    		dvd=InputDVD(); 
		    		addItemToOrder(dvd);
		    		int dd;
		    		System.out.print("Do you want to play it?(yes:1)");
		    		dd = Integer.parseInt(sc.nextLine());
		    		if(dd==1){
		    		//Yes
		    		dvd.play();
		    		}
		    	}else if(code==2){
		    		CompactDisc cd;
		    		cd = InputCD();
		    		addItemToOrder(cd);
		    		int dd;
		    		System.out.print("Do you want to play it?(yes:1)");
		    		dd = Integer.parseInt(sc.nextLine());
		    		if(dd==1){
		    			cd.play();
		    		}
		    	}else if(code ==3){
		    		Book book;
		    		book = InputBook();
		    		addItemToOrder(book);
		    	}
		    	break;
		    	}
			 	case 3:{
			 		System.out.println();
					System.out.println("Delete item by id");
					System.out.print("Enter the index you want to delete: ");
					int index = keyBoard.nextInt();
					boolean result = order.removeMedia(index);
					if(result) {
						System.out.println("Remove item to the order success");
					}else {
						System.out.println("Remove item to the order failed");
					}
			 	}break;
			 	case 4:{
			 		System.out.println("items list of order");
			 		order.printOrder();
			 	}break;
			 	default: {
			 		System.out.println("^-^  ^-^  ^-^  ^-^");
			 	}break;
			 }	 
		}while(c == 1 || c==2 || c==3 || c==4);
		
	}

}
