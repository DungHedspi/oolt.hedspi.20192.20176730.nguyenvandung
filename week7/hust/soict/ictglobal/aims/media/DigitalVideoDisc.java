package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	public DigitalVideoDisc() {
		super();
	}
	public DigitalVideoDisc(String title){
		super(title);
	}
	public DigitalVideoDisc(String category,String title){
		super(category,title);
	}
    public DigitalVideoDisc(String director,String title, String category){
		super(title,category,director);
	}
    public DigitalVideoDisc(String title, String category,String director,int length,float cost){
    	super(title,category,director,length,cost);

    }
    public boolean search(String title){
    	title= title.trim();
    	String[] token = title.split(" ");
    	boolean flag=true;
    	for(int i=0;i<token.length;i++){
    		if(title.contains(token[i])==false){
    			flag = false;
    		}
    	}
    	return flag;
    }
	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		}
}
