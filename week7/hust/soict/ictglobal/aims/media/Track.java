package hust.soict.ictglobal.aims.media;

public class Track implements Playable{
	private int length;
	private String director;
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	@Override
	
	public void play() {
		System.out.println("Playing DVD: ");
		System.out.println("DVD length: " + this.getLength());
	}

}
