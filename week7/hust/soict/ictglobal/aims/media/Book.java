package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends media {

	private List<String> authors = new ArrayList<String>();
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public boolean addAuthor(String author) {
		boolean result = false;
		int index = this.authors.indexOf(author);
		if(index == -1) {
			this.authors.add(author);
			result = true;
		}
		return result;
	}
	public boolean removeAuthor(String author) {
		boolean result = false;
		int index = this.authors.indexOf(author);
		if(index > -1) {
			this.authors.remove(author);
			result = true;
		}
		return result;
	}

}
