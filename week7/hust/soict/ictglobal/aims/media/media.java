package hust.soict.ictglobal.aims.media;

public abstract class media {
	
	private String title;
	private String category;
	private float cost;
	public media(String title){
		this.title = title;
		}
	public media(String title, String category){
		this(title);
		this.category = category;
	}
	public media(String title, String category, float cost){
		this(title, category);
		this.cost = cost;
	}
	
	public media() {
		// TODO Auto-generated constructor stub
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
}
