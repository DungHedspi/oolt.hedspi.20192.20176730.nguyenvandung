package Shopping;
import java.util.Date;
public class Order {
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders=0;
	private Date dateOrdered;
	private int qtyOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DitalVideoDisc itemsOrdered[] = new DitalVideoDisc[MAX_NUMBERS_ORDERED];
	

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public boolean addDigitalVideoDisc(DitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBERS_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}
	public DitalVideoDisc[] addDigitalVideoDisc(DitalVideoDisc [] dvdList) {
		int i;
		DitalVideoDisc[] dvdResult = new DitalVideoDisc[100];
		for(i=0; dvdList[i]!=null && this.qtyOrdered<MAX_NUMBERS_ORDERED; i++) {
			itemsOrdered[qtyOrdered++] = dvdList[i];
		}
		if(i!=dvdList.length) {
			for(int c = 0 ; c < dvdList.length && i<dvdList.length ; c++) {
				dvdResult[c] = dvdList[i++];
			}
		}
		return dvdResult;
	}
	public DitalVideoDisc[] addDigitalVideoDisc(DitalVideoDisc disc1, DitalVideoDisc disc2) {
		DitalVideoDisc[] dvdList = new DitalVideoDisc[3];
		dvdList[0]=disc1;
		dvdList[1]=disc2;
		return addDigitalVideoDisc(dvdList);
	}
	public boolean removeDigitalVideoDisc(DitalVideoDisc disc) { 
		int c, i;
		boolean result = false;
		for( c = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[c] = this.itemsOrdered[i];
				c++;
			}
		}
		if(this.qtyOrdered != c) {
			this.setQtyOrdered(c);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}
	
	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	private void setDateOrdered() {
		this.dateOrdered = new Date();
	}
	public Order() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }
}
