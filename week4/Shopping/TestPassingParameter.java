package Shopping;
public class TestPassingParameter {
	  public static void main(String[] args) {
	    DitalVideoDisc jungleDVD = new DitalVideoDisc("Jungle");
	    DitalVideoDisc cinderellaDVD = new DitalVideoDisc("Cinderella");

	    swap(jungleDVD, cinderellaDVD);
	    // swap only change title of DVD's copy value
	    System.out.println("Jungle DVD title: " + jungleDVD.getTitle());
	    System.out.println("Cinderella DVD title: " + cinderellaDVD.getTitle());
	    changeTitle(jungleDVD, cinderellaDVD.getTitle());
	    // changeTitle set new name for DVD
	    System.out.println("Jungle DVD title: " + jungleDVD.getTitle());
	  }

	  public static void swap(Object o1, Object o2) {
	    Object tmp = o1;
	    o1 = o2;
	    o2 = tmp;
	  }

	  public static void changeTitle(DitalVideoDisc dvd, String title) {
	    String oldTitle = dvd.getTitle();
	    dvd.setTitle(title);
	    dvd = new DitalVideoDisc(oldTitle);
	  }
	}
