package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;
import java.util.Date;

import hust.soict.ictglobal.aims.media.media;
public class Order {
	public static final int MAX_LIMITED_ORDERS = 5;
	private Date dateOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private ArrayList<media> itemsOrdered = new ArrayList<media>();
	
	public boolean addMedia(media disc) {
		boolean result = false;
		int index = this.itemsOrdered.indexOf(disc);
		if(index == -1) {
			this.itemsOrdered.add(disc);
			result = true;
		}
		return result;
	}
	
	public boolean removeMedia(int index) {
		boolean result = false;
		media disc = this.itemsOrdered.get(index - 1);
		if(disc != null) {
			this.itemsOrdered.remove(index-1);
			result = true;
		}
		return result;
	}
	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < this.itemsOrdered.size(); i++) {
           sum += this.itemsOrdered.get(i).getCost();
        }
		return sum;
	}
	public void printOrder() {
	    System.out.println("Order");
	    System.out.println("Date: " + dateOrdered);
	    System.out.println("Ordered items:");
	    for (int i = 0; i < this.itemsOrdered.size(); i++) {
	    	 System.out.println(i + 1 + ". DVD - " 
	    			 + this.itemsOrdered.get(i).getTitle() + " - " 
	    			 + this.itemsOrdered.get(i).getCategory()+ " - " 
	    			 + this.itemsOrdered.get(i).getCost() + "$");
	    }
	    System.out.println("Total cost: " + this.totalCost());
	    
	    System.out.println("****************************************");
	  }
}
