package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Book extends media {
	private String content;
	private List<String> authors = new ArrayList<String>();
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrquency;
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public boolean addAuthor(String author) {
		boolean result = false;
		int index = this.authors.indexOf(author);
		if(index == -1) {
			this.authors.add(author);
			result = true;
		}
		return result;
	}
	public boolean removeAuthor(String author) {
		boolean result = false;
		int index = this.authors.indexOf(author);
		if(index > -1) {
			this.authors.remove(author);
			result = true;
		}
		return result;
	}

	@Override
	public int compareTo(Object obj) {
		Book book = new Book();
		book = (Book)obj;
		return this.getTitle().compareTo(book.getTitle());
	}

	public List<String> getContentTokens() {
		return contentTokens;
	}

	public void setContentTokens(List<String> contentTokens) {
		this.contentTokens = contentTokens;
	}

	public Map<String, Integer> getWordFrquency() {
		return wordFrquency;
	}

	public void setWordFrquency(Map<String, Integer> wordFrquency) {
		this.wordFrquency = wordFrquency;
	}
	public String toString() 
	{	
		String tokenList = "";
		String frequencyList = "";
		int i = 0;
		

		java.util.Iterator<String> iterator = this.contentTokens.iterator();
		while(iterator.hasNext())
		{
			if(i%10 == 0)
				tokenList = tokenList + "\n";
			
			tokenList = tokenList + "  " + iterator.next();
			i++;
		}
		tokenList = tokenList + "\n";
		
		for(Map.Entry<String, Integer> entry: this.wordFrquency.entrySet())
		{
			frequencyList += entry.getKey() + ": " + Integer.toString(entry.getValue()) + "\n";
		}
		
		return "------------------------------------------------\n" +
			   "Title: " + this.getTitle() + "\n" +
			   "Category: " + this.getCategory() + "\n" +
			   "Authors: " + this.authors.toString() + "\n" + 
			   "Cost: " + Float.toString(this.getCost()) + "\n" +
			   "Original Content: " + this.content + "\n" +
			   "The content lenght: " + Integer.toString(this.contentTokens.size()) + "\n" +
			   "The token list: "  + "\n" + 
			   tokenList + 
			   "The word frequency: " + "\n" +
			   frequencyList;
		
	}
	
	public void processContent()
	{
		String Reg = "[ ,.]";
		String[] str = this.content.split(Reg);
		int i=0;
		int length = str.length;
		while(i<length)
		{
			System.out.println(str[i]);
			this.contentTokens.add(str[i]);
			i++;
		}
		
		// sort a->z
		Collections.sort(this.contentTokens);
		
		// count the frequency of eacdsh token
		this.wordFrquency = new java.util.HashMap<String, Integer>();
		java.util.Iterator<String> iterator = this.contentTokens.iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			if(this.wordFrquency.containsKey(key)) // update
			{
				
				int value = this.wordFrquency.get(key) + 1;
				this.wordFrquency.remove(key);
				this.wordFrquency.put(key, value);
			}
			else
			{
				this.wordFrquency.put(key, 1); 
			}
		}	
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}

}
