package hust.soict.ictglobal.aims.media;
@SuppressWarnings("unused")
public class Disc extends media implements Comparable {
	private int length;
	private String director;
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
    public Disc(){
		super();
	}
	public Disc(String title){
		super(title);
	}
	public Disc(String category,String title){
		super(category,title);
	}
    public Disc(String director,String title, String category){
		super(title,category);
		this.director= director;
	}
    public Disc(String title, String category,String director,int length,float cost){
    	super(title,category,cost);
    	this.director =director;
    	this.length =length;
    }
    @Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Disc newobj = new Disc();
		newobj = (Disc)obj; 
		return this.getTitle().compareTo(newobj.getTitle());
	}
}
