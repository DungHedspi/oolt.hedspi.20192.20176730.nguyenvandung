package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;
@SuppressWarnings("unused")
public class CompactDisc extends Disc implements Playable{
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public boolean addTrack(Track disc) {
		boolean result = false;
		int index = tracks.indexOf(disc);
		if(index < 0) {
			tracks.add(disc);
			result = true;
		}
		return result;
	}
	public boolean removeTrack(Track disc) {
		boolean result = false;
		int index = tracks.indexOf(disc);
		if(index >= 0) {
			tracks.remove(index);
			result = true;
		}
		return result;
	}
	public int getLength() {
		int length = 0;
		for(int i=0; i<tracks.size();i++) {
			length += tracks.get(i).getLength();
		}
		return length;
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for(int i=0; i<tracks.size();i++) {
			tracks.get(i).play();
		}
	}

}
