package hust.soict.ictglobal.aims.Aims;
import hust.soict.ictglobal.aims.media.*;
import java.util.Scanner;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book();
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter title: ");
		String title = sc.nextLine();
		System.out.print("Enter category: ");
		String category = sc.nextLine();
		System.out.print("Enter cost: ");
		float cost = Float.parseFloat(sc.nextLine());
		int flag = 1, i=1;
		do{
			if(flag==1){
				System.out.print("Author "+i+": ");
				String author= sc.nextLine();
				book.addAuthor(author);
				i++;
			}		
			System.out.print("Enter flag= ");
			flag= Integer.parseInt(sc.nextLine());
		}while(flag==1);
		System.out.print("Enter content: ");
		String content = sc.nextLine();
		
		book.setTitle(title);
		book.setCategory(category);
		book.setCost(cost);
		
		book.setContent(content);
		
		System.out.println(book);

	}
}
